// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var app = express();

request.defaults({"proxy": "http://192.70.36.24:3128", "https-proxy": "http://192.70.36.24:3128"});

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

// Proxy requests to Wikipedia API
app.get('/api', function(req, res) {
	request("https://fr.wikipedia.org/w/api.php?" + req.url.split("?")[1]).pipe(res);
});
//app.get('/api', function(req, res) {
//	request("https://fr.wikipedia.org/w/api.php?" + req.url.split("?")[0], function (error, response) { console.log ("Error: " + error); return response}).pipe(res);
//});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('L’API Wikipedia est accessible sur http://localhost:' + port + "/api");
});
